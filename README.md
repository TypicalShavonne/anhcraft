### Hi there 👋

- I'm Duy Anh, currently a student, gonna be 18 next year.
- I'm preparing for the univerity entrance exam 📚
- ☕ I love programming, and especially game-modding
- I have been a Minecraft/Bukkit modder for 4 years 🙈
- 🌱 I’m currently learning Rust as well as other technologies 
- 📫 How to reach me: [@dyahtt](https://twitter.com/dyahtt) | Discord: duyanh#1646

### Stats
<div align="center">
  <img height="180em" src="https://github-readme-stats.vercel.app/api?username=anhcraft&count_private=true&show_icons=true&theme=dark" />
  <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=anhcraft&theme=dark&layout=compact&langs_count=6" />
</div>
